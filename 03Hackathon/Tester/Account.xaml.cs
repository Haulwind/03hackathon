﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Tester
{
    public partial class Account : ContentPage
    {
        public Account()
        {
            InitializeComponent();

            LastName.Text = Utilities.p.lastName;
            FirstName.Text = Utilities.p.firstName;
            MiddleName.Text = Utilities.p.middleName;
            //birthDate.SetValue( System.DateTime.Now.ToString("yyyy-MM-dd");

            // var SelectedAuthor = Gender.v(a => a.Id == author_id);

            //Gender.set
            PassportNom.Text = Utilities.p.passportNum;
            PassportSerie.Text = Utilities.p.passportSerie;
            SNILS.Text = Utilities.p.snils;
            TEL.Text = Utilities.p.tel;
            TEL2.Text = Utilities.p.tel2;
            Address.Text = Utilities.p.address;
            Chronic.Text = Utilities.p.chronic;
        }
    }
}
