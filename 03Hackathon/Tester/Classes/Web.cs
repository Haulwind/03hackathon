﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Tester
{
    public static class Web
    {
        static HttpClient _client;

        public static ResponsePacket dataPacket;

        static Web()
        {
            _client = new HttpClient();
        }


      //  public static async Task<string> Authorization(string email, string password, string resource)
       // {
        //    try
       //     {


         //       var content = new MultipartFormDataContent();
         //       content.Headers.ContentType.MediaType = "multipart/form-data";

         //       content.Add(new StringContent(email), "email");
         //       content.Add(new StringContent(password), "password");

         //       var response = _client.PostAsync(resource, content).Result;

         //       response.EnsureSuccessStatusCode();
         //       var contentdata = await response.Content.ReadAsStringAsync();


          //      return contentdata;

          //  }
          //  catch (Exception ex)
          //  {
          //      Console.WriteLine(@"ERROR {0}", ex.Message);
          //      return string.Empty;
           // }
        //}


        public static async Task<string> MakeGetRequest(string resource)
        {
            try
            {
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(resource),
                    Method = HttpMethod.Get,
                };
                var response = await _client.SendAsync(request);

                return await response.Content.ReadAsStringAsync();

            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
