﻿using System;
namespace Tester
{
    [Serializable]
    public class ResponsePacket
    {
        public bool status;
        public string data;
    }
}
