﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace Tester
{
    public static class Utilities
    {
        static string configFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "config.json");
        public static ProfileClass p;

        static Utilities()
        {
            LoadConfig();

            // File.Delete(configFile);
        }

        public static void LoadConfig()
        {
            if (File.Exists(configFile))
            {

                p = JsonConvert.DeserializeObject<ProfileClass>(File.ReadAllText(configFile));
            }
            else
            {
                CleanConfig();
            }
        }

        public static void SaveConfig()
        {
            File.WriteAllText(configFile, JsonConvert.SerializeObject(p));
        }

        public static void CleanConfig()
        {
            p = new ProfileClass()
            {
                address = string.Empty, birthDate = string.Empty, chronic = string.Empty, firstName = string.Empty,
                gender = string.Empty, lastName = string.Empty, snils = string.Empty, middleName = string.Empty, passportNum = string.Empty,
                passportSerie = string.Empty, tel = string.Empty, tel2 = string.Empty
            };

            SaveConfig();
        }
    }


}
